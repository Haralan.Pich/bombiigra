import java.awt.Button;
import java.awt.Frame;
import java.awt.Label;



public class GameWindow extends Frame {
	GameWindowListener gameWindowListener;
	GameButtonActionListener gameButtonActionListener;
	GamePanel gamePanel;
	Map map;
	Button restart;
	static Label minesAmount;
	Label youWon;
	
	GameWindow(Map m) {
		gameButtonActionListener = new GameButtonActionListener(this);		
		map  = m;
		minesAmount = new Label();
		minesAmount.setBounds(550, 30, 50, 30);		
		this.add(minesAmount);
		
		restart = new Button();
		restart.setBounds(600, 30, 50, 30);
		restart.setActionCommand(null);
		restart.setLabel("restart");
		this.add(restart);
		
		youWon = new Label("You have won");
		youWon.setBounds(580, 60, 100, 30);		
		
		
		
		gameWindowListener = new GameWindowListener();
		addWindowListener(gameWindowListener);
		restart.addActionListener(gameButtonActionListener);
		
	}
	public void createMapPanel() {
		gamePanel = new GamePanel(this);
		gamePanel.setBounds(275, 0, this.map.getSizeX()*20+280, this.map.getSizeY()*20+110);
		this.add(gamePanel);
		gameWindowListener.gameMouseListener = gamePanel.gameMouseListener;
	
	}
	
}
