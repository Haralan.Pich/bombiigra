import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class GameMouseListener implements MouseListener {
	GamePanel gamePanel;
	boolean isGenerated = false;
	
	public boolean isGenerated() {
		return isGenerated;
	}
	public void setGenerated(boolean isGenerated) {
		this.isGenerated = isGenerated;
	}
	
	public GameMouseListener(GamePanel g) {
		gamePanel = g;
		
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		if(Minefield.haslost==false) {
			if(e.getX()>275 && e.getY()>100 && e.getX()<gamePanel.gameWindow.map.getSizeX()*20+275 && e.getY()<gamePanel.gameWindow.map.getSizeY()*20+100  ) {
				
				int squareX = (e.getX() - 275) / 20;
				int squareY = (e.getY() - 100) / 20;
				
				if(e.getButton()==1)	{
					
				if(isGenerated == false) {
				
					isGenerated = true;
					
					Minefield.generateMinefield(squareX,squareY);
					
					gamePanel.repaint();
				}
				
				else {
					
					Minefield.clickedOnTheField(squareX,squareY,1);
					GameWindow.minesAmount.setText(Minefield.getCount() + "");
					gamePanel.repaint();
					}
				}
				else if(e.getButton()==3) {
					
					Minefield.clickedOnTheField(squareX,squareY,3);					
					gamePanel.repaint();
					GameWindow.minesAmount.setText(Minefield.getCount() + "");
				}
					
					
					
					
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

}
