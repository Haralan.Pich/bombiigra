import java.awt.Graphics;
import java.awt.Panel;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


import javax.imageio.ImageIO;

public class GamePanel extends Panel {
	
	GameWindow gameWindow;
	GameMouseListener gameMouseListener;
	
	public GamePanel(GameWindow g) {
		
		Minefield.gamePanel=this;
		gameWindow = g;
		gameMouseListener  =  new GameMouseListener(this);
		addMouseListener(gameMouseListener);
	}
	public void paint(Graphics g) {
		File mine = new File("Mine.png");
		File one = new File("one.png");
		File dve = new File("dve.png");
		File tri = new File("tri.png");		
		File chetiri = new File("chetiri.png");		
		File pet = new File("pet.png");
		File shest = new File("shest.png");
		File sedem = new File("sedem.png");
		File osem = new File("osem.png");		
		File unknown = new File("Unknown.png");
		File safePlace = new File("safePlace.png");
		File flag = new File("Flag.png");
		
		int sizexInPixels = gameWindow.map.getSizeX() * 20;
		int sizeyInPixels = gameWindow.map.getSizeY() * 20;
		
			for (int i = 0; i <= gameWindow.map.getSizeX(); i++) {				
				g.drawLine(275 + i * 20, 100, 275 + i * 20, 100 + sizeyInPixels);
			}
			for (int i = 0; i <= gameWindow.map.getSizeY(); i++) {
				g.drawLine(275, 100 + i * 20, 275 + sizexInPixels, 100 + i * 20);
			}
			this.requestFocus();
			
			
			if(this.gameMouseListener.isGenerated==true) {
				BlockValues  minefield[][] = Minefield.returnMines();
				for (int i = 0; i < gameWindow.map.getSizeY(); i++) {
					for (int j = 0; j < gameWindow.map.getSizeX(); j++) {
						try {
							
							if(minefield[j][i] == BlockValues.flag || minefield[j][i] == BlockValues.flaggedMine) {
								BufferedImage flagI = ImageIO.read(flag);
								
								g.drawImage(flagI, 275+j*20, 100 + i * 20,20, 20, null);
							}
							else if(minefield[j][i] == BlockValues.unknown || minefield[j][i] == BlockValues.Mine ) {
								BufferedImage unknownI = ImageIO.read(unknown);
								
								g.drawImage(unknownI, 275+j*20, 100 + i * 20,20, 20, null);
							}
							else if((minefield[j][i]  != BlockValues.unknown && minefield[j][i] != BlockValues.Mine) || minefield[j][i]  == BlockValues.noMine) {
								switch (minefield[j][i]) {
								case one:
									BufferedImage oneI = ImageIO.read(one);
									g.drawImage(oneI, 275+j*20, 100 + i * 20,20, 20, null);
									break;
								case dve:
									BufferedImage dveI = ImageIO.read(dve);
									g.drawImage(dveI, 275+j*20, 100 + i * 20,20, 20, null);
									break;
								case tri:
									BufferedImage triI = ImageIO.read(tri);
									g.drawImage(triI, 275+j*20, 100 + i * 20,20, 20, null);
									break;
								case chetiri:
									BufferedImage chetiriI = ImageIO.read(chetiri);
									g.drawImage(chetiriI, 275+j*20, 100 + i * 20,20, 20, null);
									break;
								case pet:
									BufferedImage petI = ImageIO.read(pet);
									g.drawImage(petI, 275+j*20, 100 + i * 20,20, 20, null);
									break;
								case shest:
									BufferedImage shestI = ImageIO.read(shest);
									g.drawImage(shestI, 275+j*20, 100 + i * 20,20, 20, null);
									break;
								case sedem:
									BufferedImage sedemI = ImageIO.read(sedem);
									g.drawImage(sedemI, 275+j*20, 100 + i * 20,20, 20, null);
									break;
								case osem:
									BufferedImage osemI = ImageIO.read(osem);
									g.drawImage(osemI, 275+j*20, 100 + i * 20,20, 20, null);
									break;
								default:
									BufferedImage safePlaceI = ImageIO.read(safePlace);
									g.drawImage(safePlaceI, 275+j*20, 100 + i * 20,20, 20, null);
									break;
								}
							}
							if(Minefield.showMines==true) {
									if(minefield[j][i] == BlockValues.Mine) {
									BufferedImage mineI = ImageIO.read(mine);
									
									g.drawImage(mineI, 275+j*20, 100 + i * 20,20, 20, null);
									}
							}
							//g.drawImage(img,50+gameWindow.map.razboinik.getPositionX()*50, 50+ gameWindow.map.razboinik.getPositionY()*50, 50, 50,null);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				}
			}
		
	}
	public long timer() {
		long startTime = System.currentTimeMillis();
		long elapsedTime = System.currentTimeMillis() - startTime;
		long elapsedSeconds = elapsedTime / 1000;
		long secondsDisplay = elapsedSeconds % 60;
		
		
		
		return secondsDisplay;
	}
	
}
