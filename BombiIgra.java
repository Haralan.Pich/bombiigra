
public class BombiIgra {

	public static void main(String[] args) {
		Map m = new Map(8,15,20);		
		Minefield mi = new Minefield(m);
		GameWindow f = new GameWindow(m);
		
		f.createMapPanel();
		f.setLayout(null);
		f.setSize(f.getMaximumSize());
		f.setVisible(true);
	}

}
