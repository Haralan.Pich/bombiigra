
import java.util.Random;

public  class Minefield {
	static GamePanel gamePanel;
	static Map map;
	static BlockValues minefield[][];
	static boolean wereMinesAdded = false;
	static boolean haslost = false;
	static boolean showMines = false;
	static int count=0;
	static int realcount=0;
	public static int getCount() {
		return count;
	}
	public Minefield(Map m) {
		map=m;
		
	}
	public Minefield() {		
		
	}
	public static void generateMinefield(int startX,int startY) {
		
		minefield = new BlockValues [map.getSizeX()][map.getSizeY()];
		if(startX-1>0 && startY-1>0) {
		minefield[startX-1][startY-1] = BlockValues.noMine;
		}
		if(startX-1>0) {
		minefield[startX-1][startY] = BlockValues.noMine;
		}
		if(startY-1>0) {
		minefield[startX][startY-1] = BlockValues.noMine;
		}
		if(startX+1<map.getSizeX()) {
		minefield[startX+1][startY] = BlockValues.noMine;
		}
		if( startY+1<map.getSizeY()) {
		minefield[startX][startY+1] = BlockValues.noMine;
		}
		if(startX+1<map.getSizeX() && startY+1<map.getSizeY()) {
		minefield[startX+1][startY+1] = BlockValues.noMine;
		}
		if(startX-1>0 && startY+1<map.getSizeY()) {
		minefield[startX-1][startY+1] = BlockValues.noMine;
		}
		if(startX+1<map.getSizeX() && startY-1>0) {
		minefield[startX+1][startY-1] = BlockValues.noMine;
		}
		
		minefield[startX][startY] = BlockValues.noMine;
		
		
		
		
		
		addMines(minefield);
		
		
	}
	public static void addMines(BlockValues m[][]) {
		
		int minesgenerated=0;
		for(int i =0;i<map.getSizeY();i++) {
			for (int j = 0; j < map.getSizeX(); j++) {
				while(map.getMineCount()>0 && minesgenerated!=map.getMineCount()) {
					
					Random rand = new Random();
					int randy = rand.nextInt(map.getSizeY());
					int randx = rand.nextInt(map.getSizeX());
					// BlockValues.noMine safe place MIne mini
					if(minefield[randx][randy] != BlockValues.noMine && minefield[randx][randy] != BlockValues.Mine) {
						minesgenerated++;
						minefield[randx][randy] = BlockValues.Mine;
					}
				}
				
					 
			}
		}
		
		addUnknownPlaces(minefield);
		
	}
		public static void addUnknownPlaces(BlockValues m[][]) {
			
			
			for(int i =0;i<map.getSizeY();i++) {
				for (int j = 0; j < map.getSizeX(); j++) {
					    if(minefield[j][i] == BlockValues.noMine) {
					 areThereBombs(j,i);
					}
					    else if (minefield[j][i] == null ) {
						minefield[j][i] = BlockValues.unknown;
					}
					
						 
				}
			}
			addNumersNextToSafePlaces();
			
		}
		public static void printMines(BlockValues m[][]) {
			
			for(int i =0;i<map.getSizeY();i++) {
				for (int j = 0; j < map.getSizeX(); j++) {
					System.out.print(m[j][i]+" ");
				}
				System.out.println();
			}
		}
		public static  BlockValues[][] returnMines() {
			
			return  minefield;
			
		}
		public static void setMinefield(BlockValues[][] minefield) {
			Minefield.minefield = minefield;
		}
		
		public static void areThereBombs(int j, int i) {
			
			
					int minesAroundBlocksCount=0;
					if(j-1>=0 && i-1>=0 && (minefield[j-1][i-1]==BlockValues.Mine || minefield[j-1][i-1]==BlockValues.flaggedMine)) {
						minesAroundBlocksCount++;
					}
					if(j-1>=0 && (minefield[j-1][i]==BlockValues.Mine || minefield[j-1][i]==BlockValues.flaggedMine)) {
						minesAroundBlocksCount++;
					}
					if(i-1>=0 && (minefield[j][i-1]==BlockValues.Mine || minefield[j][i-1]==BlockValues.flaggedMine)) {
						minesAroundBlocksCount++;
					}
					if(j+1<map.getSizeX() && (minefield[j+1][i]==BlockValues.Mine || minefield[j+1][i]==BlockValues.flaggedMine)) {
						minesAroundBlocksCount++;
					}
					if( i+1<map.getSizeY() && (minefield[j][i+1]==BlockValues.Mine || minefield[j][i+1]==BlockValues.flaggedMine)) {
						minesAroundBlocksCount++;
					}
					if(j+1<map.getSizeX() && i+1<map.getSizeY() && (minefield[j+1][i+1]==BlockValues.Mine || minefield[j+1][i+1]==BlockValues.flaggedMine)) {
						minesAroundBlocksCount++;
					}
					if(j-1>=0 && i+1<map.getSizeY() && (minefield[j-1][i+1]==BlockValues.Mine || minefield[j-1][i+1]==BlockValues.flaggedMine)) {
						minesAroundBlocksCount++;
					}
					if(j+1<map.getSizeX() && i-1>=0 && (minefield[j+1][i-1]==BlockValues.Mine || minefield[j+1][i-1]==BlockValues.flaggedMine)) {
						minesAroundBlocksCount++;
					}					
					convertToNumbers(minesAroundBlocksCount,j,i);
					
			
		}
		public static void convertToNumbers(int mines, int x, int y) {
			
			switch (mines) {
			case 0: 
				minefield[x][y] = BlockValues.noMine;
				break;
			case 1:
				minefield[x][y] = BlockValues.one;
				break;
			case 2:
				minefield[x][y] = BlockValues.dve;
				break;
			case 3:
				minefield[x][y] = BlockValues.tri;
				break;
			case 4:
				minefield[x][y] = BlockValues.chetiri;
				break;
			case 5:
				minefield[x][y] = BlockValues.pet;
				break;
			case 6:
				minefield[x][y] = BlockValues.shest;
				break;
			case 7:
				minefield[x][y] = BlockValues.sedem;
				break;
			case 8:
				minefield[x][y] = BlockValues.osem;
				break;
						
			default:
				break;
				
			
				
			}
			
			gamePanel.repaint();
			
		}
		public static void addNumersNextToSafePlaces() {
			
				for(int i =0;i<map.getSizeY();i++) {
					for (int j = 0; j < map.getSizeX(); j++) {
						if(minefield[j][i]== BlockValues.noMine) {
							
								if(j-1>=0 && i-1>=0) {
									areThereBombs(j-1,  i-1);
									
								
								}
								if(j-1>=0) {
									areThereBombs(j-1,  i);
								
								}
								if(i-1>=0) {
									areThereBombs(j,  i-1);
								
								}
								if(j+1<map.getSizeX()) {
									areThereBombs(j+1,  i);
								
								}
								if( i+1<map.getSizeY()) {
									areThereBombs(j,  i+1);
								
								}
								if(j+1<map.getSizeX() && i+1<map.getSizeY()) {
									areThereBombs(j+1,  i+1);
								
								}
								if(j-1>=0 && i+1<map.getSizeY()) {
									areThereBombs(j-1,  i+1);
								
								}
								if(j+1<map.getSizeX() && i-1>=0) {
									areThereBombs(j+1,  i-1);
								}
								
						   
						}
					}
				}
				
		}
		public  static void clickedOnTheField(int x, int y, int button) {
			
			
			if(button==1) {
				if(minefield[x][y]==BlockValues.Mine ||  minefield[x][y]==BlockValues.flaggedMine) {
					 showMines = true;
					 haslost = true;
					
				}
				else if(minefield[x][y]==BlockValues.noMine) {
					addNumersNextToSafePlaces();
				}
				else if(minefield[x][y]==BlockValues.unknown) {
					areThereBombs(x,y);
				}
				else if(minefield[x][y]==BlockValues.flag) {					
					count--;
					areThereBombs(x,y);
				}
				
				
				else  {
					areThereBombs(x,y);
				}
				
			}
			else if(button==3) {
				if(minefield[x][y]==BlockValues.Mine ) {
					realcount++;
					count++;
					minefield[x][y]=BlockValues.flaggedMine;
					if(realcount==map.getMineCount()) {
						gamePanel.gameWindow.add(gamePanel.gameWindow.youWon);
					}
					
				}
				else if(minefield[x][y]==BlockValues.flag ||  minefield[x][y]==BlockValues.flaggedMine) {
					if( minefield[x][y]==BlockValues.flaggedMine) {
						realcount--;
						count--;
					}
					else if( minefield[x][y]==BlockValues.flag) {
						count--;
					}
					minefield[x][y]=BlockValues.unknown;
					
				}
				else if(minefield[x][y]==BlockValues.unknown) {
					count++;
					minefield[x][y]=BlockValues.flag;
				}
			}
			
		}
		
}
