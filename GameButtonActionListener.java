import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class GameButtonActionListener implements ActionListener {
	GameWindow  gameWindow;
	GameMouseListener gameMouseListener;
	
	GameButtonActionListener(GameWindow gw) {
		gameWindow = gw;
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand() == "restart") {			
			Minefield.minefield =null;
			Minefield.haslost=false;
			Minefield.showMines = false;
			gameWindow.gamePanel.gameMouseListener.isGenerated=false;			
			Minefield.realcount=0;
			Minefield.count=0;
			
			gameWindow.repaint();
			
			//Minefield.generateMinefield(gameWindow.map.getSizeX(),gameWindow.map.getSizeY());
			
		}
		
	}
}