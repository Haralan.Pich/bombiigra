
public class Map {
	private int sizeX;
	private int sizeY;
	private int mineCount;
	Map(int x, int y, int m) {
		sizeX = x;
		sizeY = y;
		mineCount =m;
	}
	public int getSizeX() {
		return sizeX;
	}
	public void setSizeX(int sizeX) {
		this.sizeX = sizeX;
	}
	public int getSizeY() {
		return sizeY;
	}
	public void setSizeY(int sizeY) {
		this.sizeY = sizeY;
	}
	public int getMineCount() {
		return mineCount;
	}
	public void setMineCount(int mineCount) {
		this.mineCount = mineCount;
	}
}
