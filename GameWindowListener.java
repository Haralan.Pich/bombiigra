import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

public class GameWindowListener implements WindowListener  {
	GameMouseListener gameMouseListener;
	GameWindowListener() {
		
	}
	@Override
	public void windowOpened(WindowEvent e) {
		
			FileInputStream fis;
			try {
				fis = new FileInputStream("Minefield.bin");
				ObjectInputStream ois = new ObjectInputStream(fis);
				
				BlockValues[][] minefield = (BlockValues[][]) ois.readObject();
				Minefield.setMinefield(minefield);
				
				if(minefield.length!=0) {gameMouseListener.setGenerated(true); }
				ois.close();
				fis.close();
			} catch (IOException | ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
			
		

		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		
		try {
			FileOutputStream fos = new FileOutputStream("Minefield.bin");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(Minefield.returnMines());
			oos.close();
			fos.close();
		} catch (FileNotFoundException ex) {
			//ex.printStackTrace();
		} catch (IOException ex) {
			//ex.printStackTrace();
		}
		if(Minefield.haslost==true) {
			try {
				Files.delete(Paths.get("Minefield.bin"));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
		}
		System.exit(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

}
